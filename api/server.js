import express from 'express'
import cookieParser from 'cookie-parser';
import cors from 'cors'
import routerAuth from './routers/auth.js'
import multer from 'multer';
import { db } from './database/db.js';
import jwt from 'jsonwebtoken';
import path from 'path';
import routerSession from  './routers/sessionRouter.js'
import routerUser  from './routers/user.js'
import routerLogin from './routers/login.js'
import routerImc from "./routers/imc.js"
const app= express();
app.use(express.json())
app.use(cors())
app.use(cookieParser())



app.use(express.static('public'))

const storage = multer.diskStorage({
    destination : function (req, file, cb){
        cb(null,"public/upload");
    },
    filename: function (req, file, cb){
        cb(null, Date.now() + path.extname( file.originalname));
    },
});
const upload = multer({storage: storage});


app.post('/api/upload', upload.single('file'), (req, res) => {
    if (!req.file) {
      return res.status(400).json({ error: 'No file uploaded' });
    }
  
   const token= req.cookies.access_token;
   if (!token) return res.status(401).json("Not authenticated!");
    jwt.verify(token,"jwtKey",(err,userInfo) =>{
        if (err) return res.status(403).json("Token is not valid!");
        const userId= userInfo.id;
        const profilePictureName = req.file.filename;
        console.log(userId)
        // Update the user's profile picture in the database
        const updateQuery = 'UPDATE users SET image = ? WHERE id = ?';
        const updateValues = [profilePictureName, userId];
 
    db.query(updateQuery, updateValues, (err, result) => {
      if (err) {
        console.error('Failed to update profile picture:', err);
        return res.status(500).json({ error: 'Failed to update profile picture' });
      }
      // Profile picture updated successfully
      res.status(200).json({ message: 'Profile picture updated' });
    });
  });
});




app.post('/api/imc', (req, res) => {
    const { imc } = req.body;
  
    // Insert BMI into the database
    const insertQuery = 'INSERT INTO imc (imc) VALUES (?)';
    const insertValues = [imc];
  
    db.query(insertQuery, insertValues, (err, result) => {
      if (err) {
        console.error('Failed to insert IMC record:', err);
        return res.status(500).json({ error: 'Failed to insert IMC record' });
      }
      console.log('IMC record inserted successfully');
      return res.status(200).json({ message: 'IMC record inserted' });
    });
  });

app.use("/api/auth",routerAuth)
app.use("/api/session",routerSession)
app.use("/api/user/",routerUser)
app.use("/api/user",routerLogin)
app.use("/api/imc/",routerImc)

app.listen(8000,()=>console.log(` connected  on 8000`))