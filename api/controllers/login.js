
import { db2 } from "../database/dab2.js"
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

export const login =(req,res)=>{

    const {username,email}=req.body;
    const q= "SELECT * FROM Users WHERE username=? or email= ?";
    db2.query(q,[username,email],(error,data)=>{
        if(error) return res.status(500).json(error);
        if(data.length===0) return res.status(404).json(" user not found");

        // if username exist, check password

        const isPasswordCorrect= bcrypt.compareSync(req.body.password,data[0].password)
        if(!isPasswordCorrect) return res.status(400).json(" username or password incorrect");

        const token =jwt.sign({user_id:data[0].user_id, username:data[0].username,email:data[0].email},"jwtKey")
       
        const {password, ...other}=data[0];
        
        res.cookie("access_token",token,{httpOnly:true}).status(200).json(other);
    })

}