import { db2 } from "../database/dab2.js";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt'

export const addUser=(req,res)=>{

    const {username, email}= req.body;
    const request = "SELECT * FROM users WHERE username = ? OR email = ?";
    
    db2.query(request,[username,email],(error,data)=>{
        if(error) return res.status(501).json(error);
        
        if(data.length) return res.status(409).send("user already exist");
         
        // hash password in order to add it 
        const salt=bcrypt.genSaltSync(10);
        const hash=bcrypt.hashSync(req.body.password,salt);

        const q="INSERT INTO users(`name`,`lastName`,`username`,`email`,`password`,`image`,`date`,`gender`,`height`,`weight`) VALUES(?)";
        const values=[req.body.name,req.body.lastName,req.body.username,req.body.email,hash,req.body.image,
            req.body.date,req.body.gender,req.body.height,req.body.weight]
        
        db2.query(q,[values],(error,data)=>{
            if(error) return res.status(500).json(error);
            return res.status(200).json(" user has been created")
        })
    })
}
