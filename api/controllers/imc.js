import { db2 } from "../database/dab2.js";


export const addImc = (req,res)=>{

    const {height,weight}= req.body;
    if(!height || !weight) return res.status(400).json({ error: 'Height and weight are required.' });
    // Calculate BMI
    const heightMeters = height / 100; // Convert height to meters
    const bmi = weight / (heightMeters * heightMeters);

    const query ="INSERT INTO IMC (user_id, height, weight, bmi_value) VALUES (?, ?, ?, ?)"
    db2.query(query,[req.user.user_id,height,weight,bmi],(error,data)=>{
        if(error) return res.status(501).json(error);
        if(data.length) return res.status(409).send("you already add this imc");

        res.json({ bmi });

    })

}