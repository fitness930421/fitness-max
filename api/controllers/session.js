import { db } from "../database/db.js";
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

function authenticateToken(req, res, next) {
    const token = req.cookies.access_token; // Assuming the JWT token is stored in a cookie named 'jwt_token'
  
    if (!token) return res.sendStatus(401);
  
    jwt.verify(token, jwtSecretKey, (err, user) => {
      if (err) return res.sendStatus(403);
      req.user = user;
      next();
    });
  }
  

export const addSession=(req,res,next)=>{
    const { user_id, date, duration, calories_burned } = req.body;
    const q = 'INSERT INTO Session (user_id, date, duration, calories_burned) VALUES (?, ?, ?, ?)';
    db.query(q,[user_id, date, duration, calories_burned],(error,data)=>{
        if(error) return res.status(501).json(error);
        if(data.length) return res.status(409).send("you already add this session");
    })

    

}

