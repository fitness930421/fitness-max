import { db } from '../database/db.js';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import moment from 'moment';
import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
    service:"Gmail",
    auth:{
        user:'wenet90@gmail.com',
        pass:'iiedzzwqgunwdwag'
    }
})


export const register=(req,res)=>{


    try {
        
    const request = "SELECT * FROM users WHERE username = ? OR email = ?";
    
    db.query(request,[req.body.username,req.body.email],(error,data)=>{
        if(error) return res.status(501).json(error);
        
        if(data.length) return res.status(409).send("user already exist");
         
        // hash password in order to add it 
        const salt=bcrypt.genSaltSync(10);
        const hash=bcrypt.hashSync(req.body.password,salt);

        const q="INSERT INTO users(`name`,`lastName`,`username`,`email`,`date`,`password`) VALUES(?)";
        const values=[req.body.name,req.body.lastName,req.body.username,req.body.email,req.body.date,hash]
        
        db.query(q,[values],(error,data)=>{
            if(error) return res.status(500).json(error);
            return res.status(200).json(" user has been created")
        })
    })
}
catch(err){
    console.log(err)
}
}

export const getUser=(req,res)=>{

    const q= "SELECT * FROM users WHERE id = ?";
    db.query(q,req.params.id,(err,data)=>{
        if (err) return res.status(500).json(err);

        const user=data[0];
        const {id, name, lastName,username, email, image, date}=user;
        
        const userData = {
            id,
            name,
            lastName,
            username,
            email,
            image,
            date
          };
        return res.status(200).json(userData);
       
    })
}

export const login=(req,res)=>{
    const request="SELECT * FROM users WHERE username = ?";
    db.query(request,[req.body.username],(error,data)=>{
        if(error) return res.status(500).json(error);
        if(data.length===0) return res.status(404).json(" user not found");

        // if username exist, check password

        const isPasswordCorrect= bcrypt.compareSync(req.body.password,data[0].password)
        if(!isPasswordCorrect) return res.status(400).json(" username or password incorrect");

        const token =jwt.sign({id:data[0].id},"jwtKey")
        const {password, ...other}=data[0];
        
        res.cookie("access_token",token,{httpOnly:true}).status(200).json(other);
    })
}

export const logout=(req,res)=>{
    res.clearCookie("access_token",{sameSite:"none", secure:true}).status(200).json("user has been logged out")
}

export const unSubscriber = (req,res) =>{

    const token= req.cookies.access_token;
    if(!token) return res.status(401).json("Not authenticated!");

    jwt.verify(token,"jwtKey",(err,userInfo)=>{
        if(err) return res.status(401).json(" Token is not Valid ");
        const userId = userInfo.id;

        db.query('DELETE FROM imc WHERE id = ?', [userId], (error, result) => {
            if (error) {
              console.error('Error deleting user:', error);
              res.status(500).json({ error: 'Failed to delete user' });

            } else {

                db.query('DELETE FROM users WHERE id = ?', [userId], (error, result) => {
                    if (error) {
                      console.error('Error deleting user:', error);
                      res.status(500).json({ error: 'Failed to delete user' });
                    } else {
                      console.log('User deleted successfully');
                      res.status(200).json({ message: 'User deleted successfully' });
                    }
            })}
          });

    })
} 

export const update = (req,res) =>{
    const token= req.cookies.access_token;
    if(!token) return res.status(401).json("Not authenticated!");

    jwt.verify(token,"jwtKey",(err,userInfo)=>{
        if(err) {

            console.error('Error updating user:', error);
            res.status(500).json({ error: 'Failed to update user' });

        }
        else {
            const userId = userInfo.id;
            const updatedUserData = req.body;
            updatedUserData.date = moment(req.body.date).format("YYYY-MM-DD")
            db.query('UPDATE users SET name = ?, lastName = ? , username = ?, email = ?, date = ? WHERE id = ?;', 
            
            [updatedUserData.name,updatedUserData.lastName,updatedUserData.username,updatedUserData.email,updatedUserData.date, userId], 
            
            (error, result) => {
                if (error) {
                  console.error('Error updating user:', error);
                  res.status(500).json({ error: 'Failed to update user' });
                } else {
                  console.log('User updated successfully');
                  res.status(200).json({ message: 'User updated successfully' });
                }
              });
        }
    })

}

export const forgotPassword = async(req,res)=>{

    const {email}=req.body;
    try {
        const q= "SELECT * FROM users WHERE email = ?";
        db.query(q,[email], async(err,data)=>{
            if(err){
                console.error("Error in password reset",err)
                return res.status(500).json({err:'internal server error'});
            }

            if(data.length === 0){
                return res.status(404).json({error:" user not found"});
            }

            const user = data[0]
            
            const token= await bcrypt.hash(user.id.toString(),10);
            

            const updateQuery="UPDATE users SET password_reset_token = ? WHERE id= ?";
            db.query(updateQuery,[token,user.id],async(updateError)=>{
                if(updateError){
                    console.error("Error in password reset",updateError)
                    return res.status(500).json({error:"Internal server error"})
                }
                

                const resetLink = `http://localhost:8000/api/auth/reset-password?token=${token}`;
               
                const mailOptions= {
                    from:"wenet90@gmail.com",
                    to:email,
                    subject:"password reset",
                    html:`<p>Click the following link to reset your password:</p><a href="${resetLink}">${resetLink}</a>`
                }

                await transporter.sendMail(mailOptions);

                return res.json({message:"password reset email sent"})

            })
        })
        
    } catch (error) {

        console.error("Error in password",error);
        return res.status(500).json({error:"Internal server error"})
        
    }
}


export const resetPassword = async(req,res)=>{
    const {token,password}=req.body;
    
    try {
      // Find the user by the password reset token
      const query = 'SELECT * FROM users WHERE password_reset_token = ?';
      db.query(query, [token], async (error, results) => {
        if (error) {
          console.error('Error in password reset:', error);
          return res.status(500).json({ error: 'Internal server error' });
        }
  
        if (results.length === 0) {
          return res.status(404).json({ error: 'Invalid or expired token' });
        }
  
        const user = results[0];
  
        // Update the user's password with the new password
        const hashedPassword = await bcrypt.hash(password, 10);
        const updateQuery = 'UPDATE users SET password = ?, password_reset_token = NULL WHERE id = ?';
        db.query(updateQuery, [hashedPassword, user.id], async (updateError) => {
          if (updateError) {
            console.error('Error in password reset:', updateError);
            return res.status(500).json({ error: 'Internal server error' });
          }
  
          return res.json({ message: 'Password reset successful' });
        });
      });
    } catch (error) {
      console.error('Error in password reset:', error);
      return res.status(500).json({ error: 'Internal server error' });
    }
  
}



 export const updateSession = (req, res) => {
    const sessionId = req.params.session_id;
    const { date, duration, calories_burned } = req.body;
  
    // Assuming your database table is named 'Session'
    const query = "UPDATE Session SET date = ?, duration = ?, calories_burned = ? WHERE session_id = ?";
  
    db.query(query, [date, duration, calories_burned, sessionId], (err, results) => {
      if (err) {
        console.error('Error updating session:', err);
        res.status(500).json({ error: 'Failed to update session.' });
      } else {
        res.json({ message: 'Session updated successfully.' });
      }
    });
  };


  export const deleteSession = (req, res) => {
      const token= req.cookies.access_token;
    if(!token) return res.status(401).json("Not authenticated!");
    jwt.verify(token,"jwtKey",(err,userInfo)=>{
      if(err) {
  
          console.error('Error updating user:', error);
          res.status(500).json({ error: 'Failed to update user' });
  
      }
  
    else {
        const userId = userInfo.id;
        const sessionId= userInfo.session_id
        const q = 'DELETE Session Join Users ON Session.id = Users.id'
        db.query(q,[userId,sessionId],(err,results)=>{
          if (err) throw err;
        res.json("Session has been de");
        })
        
      }
    });
  };


  export const getSession= (req, res) => {
    const token= req.cookies.access_token;
    if(!token) return res.status(401).json("Not authenticated!");
    jwt.verify(token,"jwtKey",(err,userInfo)=>{
      if(err) {
  
          console.error('Error updating user:', error);
          res.status(500).json({ error: 'Failed to update user' });
  
      }
      else {
        const userId = userInfo.id;
        const q = 'SELECT * FROM Session Join Users ON Session.id = Users.id WHERE Session.id = ?' 
         db.query(q, [userId], (err, results) => {
      if (err) throw err;
      res.json(results);
    });
  }
})}
  
   


  export const getAllSession= (req, res) => {
    const token= req.cookies.access_token;
    if(!token) return res.status(401).json("Not authenticated!");
    jwt.verify(token,"jwtKey",(err,userInfo)=>{
      if(err) {
          console.error('Error updating user:', error);
          res.status(500).json({ error: 'Failed to update user' });
  
      }
      else {
        const userId = userInfo.id;
        const q = 'SELECT * FROM Session Join Users ON Session.id = Users.id'
        db.query(q, [userId], (err, results) => {
      if (err) throw err;
      res.json(results);
    });
  }})}


export const addSession=(req,res)=>{
  const token= req.cookies.access_token;
  if(!token) return res.status(401).json("Not authenticated!");
  jwt.verify(token,"jwtKey",(err,userInfo)=>{
    if(err) {
        console.error('Error updating user:', error);
        res.status(500).json({ error: 'Failed to update user' });

    }
    else {
      const userId = userInfo.id;
      const { date, duration, calories_burned } = req.body;
      const q = 'INSERT INTO Session (id, date, duration, calories_burned) VALUES (?, ?, ?, ?)';
      db.query(q,[userId, date, duration, calories_burned],(error,data)=>{
        if(error) return res.status(501).json(error);
        if(data.length) return res.status(409).send("you already add this session");
    })
    }
  }) 
}


export const addImcr = (req,res)=>{

    const {height,weight}= req.body;
    if(!height || !weight) return res.status(400).json({ error: 'Height and weight are required.' });
    // Calculate BMI
    const heightMeters = height / 100; // Convert height to meters
    const bmi = weight / (heightMeters * heightMeters);

    const query ="INSERT INTO IMC (id, height, weight, bmi_value) VALUES (?, ?, ?, ?)"
    db.query(query,[req.user.id,height,weight,bmi],(error,data)=>{
        if(error) return res.status(501).json(error);
        if(data.length) return res.status(409).send("you already add this imc");

        res.json({ bmi });

    })

}


export const getImc= (req, res) => {
  const token= req.cookies.access_token;
  if(!token) return res.status(401).json("Not authenticated!");

  jwt.verify(token,"jwtKey",(err,userInfo)=>{
    if(err) {

        console.error('Error updating user:', error);
        res.status(500).json({ error: 'Failed to update user' });

    }
    else {
      const userId = userInfo.id;
      const q = 'SELECT * FROM IMC Join Users ON IMC.id = Users.id WHERE IMC.id= ?'
      db.query(q, [userId], (err, results) => {
        if (err) throw err;
        console.log(results)
        res.json(results);
      });
    }
  })}



export const addImc =  (req, res) => {
  const { id, height, weight, bmi_value, date } = req.body;

  // Assuming your database table is named 'IMC'
  const query ="INSERT INTO IMC (id, height, weight, bmi_value, date) VALUES (?, ?, ?, ?, ?)";

  db.query(query, [id, height, weight, bmi_value, date], (err, results) => {
    if (err) {
      console.error('Error adding IMC record:', err);
      res.status(500).json({ error: 'Failed to add IMC record.' });
    } else {
      res.json({ message: 'IMC record added successfully.' });
    }
  });
};
