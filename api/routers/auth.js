import express from 'express'
import jwt from 'jsonwebtoken'
import { register,login,logout, unSubscriber, update, getUser, forgotPassword, resetPassword, addSession, addImc, getSession, getAllSession, updateSession, getImc, deleteSession} from "../controllers/auth.js";


const router=express.Router();

function authenticateToken(req, res, next) {
    const token = req.cookies.access_token; // Assuming the JWT token is stored in a cookie named 'jwt_token'
  
    if (!token) return res.sendStatus(401);
  
    jwt.verify(token, "jwtKey", (err, user) => {
      if (err) return res.sendStatus(403);
      req.user = user;
      next();
    });
  }
  

//router.get('/:id',getUser)
router.post('/register',register)
router.post('/login',login)
router.post('/logout',logout)
router.delete('/unsubscriber',unSubscriber)
router.put('/update',update)
router.post('/forgot-password',forgotPassword)
router.post('/reset-password',resetPassword)
router.post('/session',addSession)
router.post('/imc',authenticateToken,addImc)
router.get('/session/',getSession)
router.get('/sessions/',getAllSession)
router.put('session/:session_id',updateSession)
router.get('/imc/',getImc)
router.delete('/delete',deleteSession)

export default router;
