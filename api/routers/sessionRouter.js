import express from 'express'
import jwt from 'jsonwebtoken'
import { addSession } from '../controllers/session.js';

const router= express.Router()

 function authenticateToken(req, res, next) {
    const token = req.cookies.access_token; // Assuming the JWT token is stored in a cookie named 'jwt_token'
  
    if (!token) return res.sendStatus(401);
  
    jwt.verify(token, "jwtKey", (err, user) => {
      if (err) return res.sendStatus(403);
      req.user = user;
      next();
    });
  }


router.post('/session',authenticateToken,addSession)

export default router