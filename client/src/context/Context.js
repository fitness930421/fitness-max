import  React,{ createContext,useState, useEffect, } from "react";
import axios from "axios";


export const authContext=createContext();



const Context = ({children}) => {

    // we retrieve the current user
    const[currentUser, setCurrentUser]=useState(JSON.parse(localStorage.getItem("user"))||null)
   


    const login= async (values) =>{
       const response = await  axios.post("/auth/login",values);
       setCurrentUser(response.data)
    }

  

    const logout= async (values) =>{
        await  axios.post("/auth/logout");
        setCurrentUser(null)
     }

     const unsubscriber= async (values) =>{
         await axios.delete("/auth/unsubscriber")
         setCurrentUser(null)
     }

// keep track of user every each change and set user in local storage

     useEffect(()=>{
        localStorage.setItem("user",JSON.stringify(currentUser))
        
     },[currentUser])

  return (
    <authContext.Provider value={{currentUser,login,logout,unsubscriber}}>
        {children}
    </authContext.Provider>
  )
}

export default Context
