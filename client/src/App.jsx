import { BrowserRouter,Routes,Route } from 'react-router-dom';
import React from 'react';
import './App.css';
import ProtectedRoute from './components/protected/ProtectedRoute';
import Home from './pages/home/Home';
import About from './pages/about/About';
import Services from './pages/services/Services';
import Contact from './pages/contact/Contact';
import PublicRoutes from './components/public/PublicRoutes';
import Footer from './components/footer/Footer';
import Nav from './components/navbar/Nav';
import Register from './pages/register/Register';
import Login from './pages/login/Login';
import Profile from './pages/profile/Profile';
import EditProfile from './components/edit/EditProfile';
import PasswordReset from './components/reset/PasswordReset';
import ChangePassword from './components/reset/ChangePassword';
import AddSession from './components/session/AddSession';
import UpdateSession from './components/session/UpdateSession';
import AddIMC from './components/imc/AddIMC';
import DeleteSessionButton from './components/session/DeleteSessionButton ';
import DoctorContactForm from './components/medecin/DoctorContactForm ';

function App() {
  return (
    <BrowserRouter>
    
     <Nav/>

      <Routes>

        <Route path='/' element={<ProtectedRoute/>}>
            <Route path='/' element={<Home/>}/>
            <Route path='/home' element={<Home/>}/>
            <Route path='/about' element={<About/>}/>
            <Route path='/services' element={<Services/>}/>
            <Route path='/editProfile' element={<EditProfile/>}/>
            <Route path='/addSession' element={<AddSession/>}/>
            <Route path='/editSession' element={<UpdateSession/>}/>
            <Route path='/addImc' element={<AddIMC/>}/>
            <Route path='/delete' element={ <DeleteSessionButton/>}/>
            <Route path='/doctor' element={ <DoctorContactForm/>}/>
            <Route path='/contact-us' element={<Contact/>}/> 
           
          
          
            {/* <Route path='/contact-us' element={<Contact/>}/> */}
            
            <Route path='/profile' element={<Profile/>}/>
        </Route>

       
        <Route path='/' element={<ProtectedRoute/>}>
        <Route path='/profile' element={<Profile/>}/>
            
        </Route>

        <Route path='login' element={<PublicRoutes/>}>
          <Route path='/login' element={<Login/>}/> 
      </Route>

      {/* <Route path='contact-us' element={<PublicRoutes/>}>
          <Route path='/contact-us' element={<Contact/>}/> 
      </Route> */}

      <Route path='passwordReset' element={<PublicRoutes/>}>
          <Route path='/passwordReset' element={<PasswordReset/>}/>    
      </Route>

      <Route path='passwordChange' element={<PublicRoutes/>}>
          <Route path='/passwordChange' element={<ChangePassword/>}/>    
      </Route>

      <Route path='register' element={<PublicRoutes/>}>
          <Route path='/register' element={<Register/>}/> 
      </Route> 

      </Routes>

       <Footer/> 
  
    </BrowserRouter>
  );
}

export default App;
