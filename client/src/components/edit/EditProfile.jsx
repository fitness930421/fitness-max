import React, { useContext, useState } from 'react'
import FormInput from '../form/FormInput'
import { authContext } from '../../context/Context'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import "./editprofile.css"
const EditProfile = () => {

    const {currentUser}= useContext(authContext)
    const navigate= useNavigate()
    const[value,setValues]=useState({...currentUser})
    const [file, setFile] = useState(null);
    const inputs = [
        {
          id:1,
          name:"name",
          type:"text",
          placeholder:"Name",
          errorMessage:"name should be between 3 to 6 characters and couldn't include any special character or number",
          label:"Name",
          pattern:"^[A-Za-z]{3,16}$",
          required:true
        },
        {
          id:2,
          name:"lastName",
          type:"text",
          placeholder:"Last name",
          errorMessage:"Last name should be between 3 to 6 characters and couldn't include any special character or number",
          label:"Last Name",
          pattern:"^[A-Za-z]{3,16}$",
          required:true
        },
        {
          id:3,
          name:"username",
          type:"text",
          placeholder:"username",
          errorMessage:"name should be between 3 to 6 characters and couldn't include any special character",
          label:"Username",
          pattern:"^[A-Za-z0-9]{3,16}$",
          required:true
        },
        {
          id:4,
          name:"email",
          type:"email",
          placeholder:"e-mail",
          errorMessage:"invalid email",
          label:"Email",
          required:true
        },
        {
          id:5,
          name:"birthday",
          type:"date",
          placeholder:"Birthday",
          label:"Birthday",
          
        }
       
        
      ]
      
      const onChange=(e)=>{
         setValues({...value,[e.target.name]:e.target.value})
      
      }


      const upload = async () => {
        try {
          const formData = new FormData();
          formData.append("file", file);
          const res = await axios.post("/upload", formData);
          return res.data;
        } catch (err) {
          console.log(err);
        }
      };
      const handleFileSelect = (event) => {
        setFile(event.target.files[0]);
        
      };


      const editProfile = async (e) =>{
       try {
        e.preventDefault();
         await axios.put("/auth/update/",value)
      
         navigate('/profile')
        
       } catch (error) {
        console.log(error)
       }
        
      }


 
    
  return (
   
    <section>
      <div className="container edit__container">
        
        <form action="">
        <h3>Edit Profile</h3>
        {
            inputs.map((input)=>(
                <FormInput key={input.id} {...input }  value={value[input.name] || ""} onChange={onChange} />
            ))
        }
        <button onClick={editProfile}>Edit</button>
        </form>


        <div>  
              <h3>Edit your profile picture</h3>
              <input type="file" onChange={handleFileSelect} />
              <button onClick={upload}>Upload</button>
          </div>

        </div>
    </section>
  
  )
}

export default EditProfile