import React, { useContext } from 'react';
import {Outlet, Navigate} from 'react-router-dom';
import { authContext } from '../../context/Context';



const ProtectedRoute = () => {
  const {currentUser}=useContext(authContext);
   
  return <div>
    {
      currentUser ? <Outlet/> : <Navigate to='/login'/>
    }
  </div> 
}

export default ProtectedRoute