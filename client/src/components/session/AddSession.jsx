import React, { useState } from 'react';
import axios from 'axios';
import { authContext } from '../../context/Context'
import { useContext } from 'react'
import Body from '../body/Body';
import './addsession.css'
import CalorieBurned from './CalorieBurned';
const AddSession = () => {
const {currentUser}= useContext(authContext)
  const [id, setId] = useState('');
  const [date, setDate] = useState('');
  const [duration, setDuration] = useState('');
  const [caloriesBurned, setCaloriesBurned] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    // Replace 'http://localhost:3000' with your server URL
    const serverURL = '/auth/session';


    const sessionData = {
      id: currentUser.id,
      date: date,
      duration: duration,
      calories_burned: caloriesBurned
    };

    axios.post(`${serverURL}`, sessionData)
    .then(response => {
      console.log('Session added to the database:', response.data);
      // Clear the form fields after successful submission
      setId('');
      setDate('');
      setDuration('');
      setCaloriesBurned('');
    })
    .catch(error => console.error('Error:', error));
  };

  return (
    <Body>
    <div className="add-session-container">
      <h2>Add New Session</h2>
      <form className="add-session-form" onSubmit={handleSubmit}>
        <label>
          User ID:
          <input
            type="number"
            value={currentUser.id}
            onChange={(e) => setId(parseInt(e.target.value))}
            required
          />
        </label>
        <label>
          Time of Session:
          <input
            type="datetime-local"
            value={date}
            onChange={(e) => setDate(e.target.value)}
            required
          />
        </label>
        <label>
          Duration (minutes):
          <input
            type="number"
            value={duration}
            onChange={(e) => setDuration(parseFloat(e.target.value))}
            required
          />
        </label>
        <label>
          Calories Burned:
          <input
            type="number"
            value={caloriesBurned}
            onChange={(e) => setCaloriesBurned(parseFloat(e.target.value))}
            required
          />
        </label>
        <button type="submit">Add Session</button>
      </form>
    </div>

    <div className='calorie_burned'>

      <CalorieBurned/>

    </div>
    </Body>
  );
};

export default AddSession;
