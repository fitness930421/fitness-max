import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { authContext } from '../../context/Context'
import { useContext } from 'react'
import Body from '../body/Body';
import './updatesession.css'
import CalorieBurned from './CalorieBurned';

const UpdateSession = () => {
    const {currentUser}= useContext(authContext)
  const [sessionId, setSessionId] = useState('');
  const [id, setId] = useState('');
  const [date, setDate] = useState('');
  const [duration, setDuration] = useState('');
  const [caloriesBurned, setCaloriesBurned] = useState('');

  // Fetch the existing session data when the component mounts
  useEffect(() => {
  

    const sessionToUpdateId = 1; // Replace with the ID of the session you want to update

    axios.get(`auth/session/${sessionToUpdateId}`)
    .then(response => {
      const sessionData = response.data;
      setSessionId(sessionData.session_id);
      setId(sessionData.id);
      setDate(sessionData.date);
      setDuration(sessionData.duration);
      setCaloriesBurned(sessionData.calories_burned);
    })
    .catch(error => console.error('Error:', error));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

   

    const sessionData = {
      id: currentUser.id,
      date: date,
      duration: duration,
      calories_burned: caloriesBurned
    };

    axios.put(`auth/session/${sessionId}`, sessionData)
    .then(response => {
      console.log('Session updated in the database:', response.data);
      // Clear the form fields after successful update
      setId('');
      setDate('');
      setDuration('');
      setCaloriesBurned('');
    })
    .catch(error => console.error('Error:', error));
  };

  return (
    <Body>
    <div className="update-session-container">
      <h2>Update Session</h2>
      <form  className="update-session-form" onSubmit={handleSubmit}>
        <label>
          User ID:
          <input
            type="number"
            value={id}
            onChange={(e) => setId(parseInt(e.target.value))}
            required
          />
        </label>
        <label>
          Time of Session:
          <input
            type="datetime-local"
            value={date}
            onChange={(e) => setDate(e.target.value)}
            required
          />
        </label>
        <label>
          Duration (minutes):
          <input
            type="number"
            value={duration}
            onChange={(e) => setDuration(parseFloat(e.target.value))}
            required
          />
        </label>
        <label>
          Calories Burned:
          <input
            type="number"
            value={caloriesBurned}
            onChange={(e) => setCaloriesBurned(parseFloat(e.target.value))}
            required
          />
        </label>
        <button type="submit">Update Session</button>
      </form>
    </div>
    <div className='calorie_burned'>

      <CalorieBurned/>

    </div>
    </Body>
  );
};

export default UpdateSession;
