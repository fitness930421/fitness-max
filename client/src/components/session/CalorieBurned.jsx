import React, { useState } from 'react';
import './calio.css'
const CalorieBurned = () => {
  const [weight, setWeight] = useState('');
  const [duration, setDuration] = useState('');
  const [MET, setMET] = useState('');
  const [caloriesBurned, setCaloriesBurned] = useState(0);

  // Create a function to calculate the calories burned based on MET value, weight, and duration
  const calculateCaloriesBurned = () => {
    if (weight && duration && MET) {
      // Convert duration from minutes to hours
      const durationHours = duration / 60;
      // Calculate calories burned using the formula: Calories Burned = MET * Weight in kg * Duration in hours
      const caloriesBurned = MET * weight * durationHours;
      setCaloriesBurned(caloriesBurned);
    } else {
      setCaloriesBurned(0);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    calculateCaloriesBurned();
  };

  return (
    <div className="calorie-burned-container">
        <div  className="calorie-burned-form">
      <h2>Calories Burned Calculator</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Weight (kg):
          <input
            type="number"
            value={weight}
            onChange={(e) => setWeight(parseFloat(e.target.value))}
            required
          />
        </label>
        <label>
          Duration (minutes):
          <input
            type="number"
            value={duration}
            onChange={(e) => setDuration(parseFloat(e.target.value))}
            required
          />
        </label>
        <label>
          MET Value:
          <input
            type="number"
            value={MET}
            onChange={(e) => setMET(parseFloat(e.target.value))}
            required
          />
        </label>
        <button type="submit">Calculate Calories</button>
      </form>
      {caloriesBurned > 0 && <div  className="calorie-burned-result"> Calories Burned: {caloriesBurned.toFixed(2)}</div>}
      </div>

      <div className="calorie-burned-info">
        <p>
        MET stands for Metabolic Equivalent of Task. It is a unit used to estimate the amount of energy expended during physical activity. 
        MET values are assigned to different activities based on their intensity. 
        The MET value represents the ratio of the rate of energy expended during an activity to the rate of energy expended at rest.
    <br />
For example:
    <br /> 
1 MET is the energy expenditure at rest (sitting quietly) and is equivalent to approximately 1 kcal/kg/hour.
Light activities typically have MET values between 1.5 and 2.9 (e.g., walking slowly).
Moderate activities have MET values between 3 and 5.9 (e.g., brisk walking, cycling at a moderate pace).
Vigorous activities have MET values of 6 or higher (e.g., running, intense cycling).
By multiplying the MET value of an activity with the weight of the person in kilograms and the duration of the activity in hours,
 you can estimate the calories burned during that activity. 
 It's important to note that MET values are estimates and can vary depending on factors such as a person's fitness level and body composition.
        </p>
      </div>
    </div>
  );
};

export default CalorieBurned;
