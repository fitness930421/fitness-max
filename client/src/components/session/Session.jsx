import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { authContext } from '../../context/Context'
import { useContext } from 'react'
import './session.css'
const Session = () => {

const {currentUser}= useContext(authContext) 

const [session,setSession]=useState([])


useEffect(()=>{

      axios.get(`/auth/session/`)
      .then(response => setSession(response.data))
      .catch(error => console.error('Error:', error));

},[])

const formatDate = (dateString) => {
  const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
  const datePart = dateString.substring(0, 10); // Extract the date part (YYYY-MM-DD)
  return new Intl.DateTimeFormat('en-US', options).format(new Date(datePart));
};

  return (
      <div  className="session-container">
        <h3>MA RECENTE SEANCE</h3>
    <ul className="session-list">
       {
        session.map(session => <li key={session.id}> 
        Date: {session.date},
         Duration: { session.duration} 
         minutes, Calories Burned: {session.calories_burned}</li>)
       }
    </ul>
    </div>
  )
}

export default Session