import React, { useState } from "react";
import axios from "axios";
import { authContext } from '../../context/Context'
import { useContext } from 'react'
import { useEffect } from "react";
import Body from "../body/Body";

const DeleteSessionButton = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [userID, setUserID] = useState(); 
  const {currentUser}= useContext(authContext)
  const sessionID =12
  const handleDeleteSession = async (event) => {
    event.preventDefault();
    

    try {
      setIsLoading(true);
      const response = await axios.delete(
        `/auth/session/${sessionID}`,
      );

      setIsLoading(false);

      if (response.status === 200) {
        
      } else {
       
      }
    } catch (error) {
      setIsLoading(false);
     
      console.error("Error deleting session:", error);
    }
  };

  useEffect(()=>{
    handleUserIDChange()
  },[])

  const handleUserIDChange = () => {
    setUserID(currentUser.id);
  };

  return (
    <Body>
    <div>
      <form onSubmit={handleDeleteSession}>
        
        <input
          type="text"
          name="userID"
          value={userID}
          onChange={handleUserIDChange}
          placeholder="Enter User ID"
        />
        <input type="text" name="sessionID" value={sessionID} onChange={sessionID} />
        
        <button type="submit" disabled={isLoading}>
          {isLoading ? "Deleting..." : "Delete Session"}
        </button>
      </form>
    </div>
    </Body>
  );
};

export default DeleteSessionButton;
