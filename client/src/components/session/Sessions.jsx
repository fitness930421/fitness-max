import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { authContext } from '../../context/Context'
import { useContext } from 'react'
import './session.css'
const Sessions = () => {
    const {currentUser}= useContext(authContext) 

const [session,setSession]=useState([{}])

useEffect(()=>{

    axios.get(`/auth/sessions/`)
    .then(response => setSession(response.data))
    .catch(error => console.error('Error:', error));

},[])



  return (
    <div className="session-container">
    <h3>MES SEANCES</h3>
    <ul className="session-list">
        {
          session.map(session => <li key={session.id}> 
          Date: {session.date},
          Duration:{session.duration}
          minutes, Calories Burned: {session.calories_burned}</li>)
        }
    </ul>
</div>
)
  
}

export default Sessions