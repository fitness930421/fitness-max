import React, { useState } from 'react'
import Body from '../body/Body'
import FormInput from '../form/FormInput'

const ChangePassword = () => {

    const[password,setPassword]=useState("")
    const inputs = [
        {
            id:1,
            name:"password",
            type:"password",
            placeholder:"Password",
            errorMessage:" password should be between 8 and 16 characters and include at least 1 number, 1 special character and 1 letter",
            label:"Password",
            pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
            required:true
          },
          {
            id:2,
            name:"confirmPassword",
            type:"password",
            placeholder:"Password",
            errorMessage:" password don't match ",
            label:"Password",
            pattern: password.password,
            required:true
          }
    ]

    const onChange=(e)=>{
        setPassword(({...password,[e.target.name]:e.target.value}))
      }

  return (
    <Body>

        <form >

            {
                inputs.map((input)=> (<FormInput key={input.id} {...input} value={password[input.name]} onChange={onChange} />))
            }
        </form>


    </Body>
  )
}

export default ChangePassword