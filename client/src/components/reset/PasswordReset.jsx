import React, { useState } from 'react'
import FormInput from '../form/FormInput';
import axios from 'axios';

const PasswordReset = () => {

  const [email,setEmail]=useState({email:""});
  const [error,setError]=useState();
  const [successMessage, setSuccessMessage] = useState('');

  const inputs = [
   
    {
      id:1,
      name:"email",
      type:"email",
      placeholder:"e-mail",
      errorMessage:"invalid email",
      label:"Email",
      required:true
    },

    
  ]

  const onChange=(e)=>{
    setEmail({...email, [e.target.name]:e.target.value})
  }

  const handleSubmit = async(e)=>{
    e.preventDefault();
    try {
      await axios.post("/auth/forgot-password",email)
      setError('');
      setSuccessMessage('Password reset email sent');
    } catch (error) {
      setError(error.response.data.error)
    }
    
  }


  return (
    <section>
      <div className="container forgot__container">
    
          <br/><br />
       <form >
          
        {
          inputs.map((input)=> (<FormInput key={input.id} {...input} value={email[input.name]} onChange={onChange} />))
        }
     
        <button onClick={handleSubmit}>Reset</button>
       
        
        
       
        </form>
        {error && <p className="error">{error}</p>}
        {successMessage && <p className='success'>{successMessage}</p>}
      </div>
    </section>
  )
}

export default PasswordReset