import React, { useState } from "react";
import axios from "axios";

const DoctorContactForm = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    try {
      const formData = {
        name,
        email,
        message,
      };

  
      const response = await axios.post("/api/contact-doctor", formData);

      setIsLoading(false);

      if (response.status === 200) {
        setSuccessMessage("Message sent successfully!");
        
            setName("");
        setEmail("");
        setMessage("");
      } else {
        setErrorMessage("Failed to send the message. Please try again later.");
      }
    } catch (error) {
      setIsLoading(false);
      setErrorMessage("Failed to send the message. Please try again later.");
    }
  };

  return (
    <div>
      <h2>Contact the Doctor</h2>
      {successMessage && <p style={{ color: "green" }}>{successMessage}</p>}
      {errorMessage && <p style={{ color: "red" }}>{errorMessage}</p>}
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </div>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div>
          <label htmlFor="message">Message:</label>
          <textarea
            id="message"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            required
          />
        </div>
        <button type="submit" disabled={isLoading}>
          {isLoading ? "Sending..." : "Send Message"}
        </button>
      </form>
    </div>
  );
};

export default DoctorContactForm;
