import React, { useContext } from 'react'
import './nav.css'
import {Link, useNavigate, useLocation} from 'react-router-dom';
import { links } from '../../data';
import { authContext } from '../../context/Context';
import  LogoImg from '../../assets/logoo.png';
import {RiRadioButtonLine} from 'react-icons/ri'
const Nav = () => {

    // the idea her is to not navigate user to home ************  he must be navigated to login after logging out

    const {logout, currentUser}=useContext(authContext)
    // redirect the page to some where else
    const navigate= useNavigate();
    // locate the page

    const location= useLocation()

    const logOut= () =>{
        // localStorage.removeItem("user");
        logout()
        navigate('/login')

    }

    // user profile 
  
  return (
    <nav>
        <div className='container nav__container'>

        <Link to="/" className='logo__max'> <img src={LogoImg} alt="Max Fit" /></Link>

        {currentUser && (
        <>
            {
                links.map((link)=>(
                    <Link key={link.text} to={link.path} >{link.name}</Link>
                ))
            }

            {  
            (  
                (<div><img src={currentUser.image} alt="" />  <Link to="/profile"> <small><RiRadioButtonLine/>  {currentUser.username }</small> </Link></div>)
            )
            }

            {
                location.pathname !=="/login" && (<button onClick={logOut}>LogOut</button>)  
            }
           
        </>
        )}

        {
            !currentUser && (<Link to="/login" className='btn'>  Login</Link>)
        }

        </div>

    </nav>
  )
}

export default Nav