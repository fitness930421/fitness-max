import React from 'react'
import './body.css'
const Body = ({children}) => {
  return (
    <section className='main__body'>
      <div className='container main__body-container'>
        {children}
        </div>
    </section>
  )
}

export default Body