import React, { useContext } from 'react';
import {Outlet, Navigate} from 'react-router-dom';
import { authContext } from '../../context/Context';

const PublicRoutes = () => {
    

    const {currentUser}=useContext(authContext);
    return <div>
        {
             currentUser ?  <Navigate to='/'/> : <Outlet/>  
        }
    </div> 
}

export default PublicRoutes