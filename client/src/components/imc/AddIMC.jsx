import React, { useState } from 'react';
import axios from 'axios';
import { authContext } from '../../context/Context'
import { useContext } from 'react'
import './addimc.css'
import Body from '../body/Body';
const AddIMC = () => {

  const {currentUser}= useContext(authContext) 
  const [id, setUserId] = useState('');
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [bmiValue, setBmiValue] = useState('');
  const [date, setDate] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    // Calculate BMI Value
    const heightInMeter = height / 100; // Convert height from cm to meters
    const bmiValue = weight / (heightInMeter * heightInMeter);
    setBmiValue(bmiValue);


    const imcData = {
      id: currentUser.id,
      height: height,
      weight: weight,
      bmi_value: bmiValue,
      date: date
    };

    axios.post(`auth/imc`, imcData)
    .then(response => {
      console.log('IMC record added to the database:', response.data);
      // Clear the form fields after successful submission
      setUserId('');
      setHeight('');
      setWeight('');
      setBmiValue('');
      setDate('');
    })
    .catch(error => console.error('Error:', error));
  };

  return (
    <Body>
    <div  className="add-imc-container">
      <h2>Add New IMC Record</h2>
      <form  className="add-imc-form" onSubmit={handleSubmit}>
        <label>
          User ID:
          <input
            type="number"
            value={id}
            onChange={(e) => setUserId(parseInt(e.target.value))}
            required
          />
        </label>
        <label>
          Height (cm):
          <input
            type="number"
            value={height}
            onChange={(e) => setHeight(parseFloat(e.target.value))}
            required
          />
        </label>
        <label>
          Weight (kg):
          <input
            type="number"
            value={weight}
            onChange={(e) => setWeight(parseFloat(e.target.value))}
            required
          />
        </label>
        <label>
          Date:
          <input
            type="date"
            value={date}
            onChange={(e) => setDate(e.target.value)}
            required
          />
        </label>
        <button type="submit">Add IMC Record</button>
      </form>
      
    </div>
    </Body>
  );
};

export default AddIMC;
