import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './imc.css'
const GetIMC = () => {
  const [imcRecords, setImcRecords] = useState([]);

  useEffect(() => {
   
    axios.get(`/auth/imc/`)
    .then(response => {
      const imcData = response.data;
      setImcRecords(imcData);
    })
    .catch(error => console.error('Error:', error));
  },[]);

  return (
    <div className='imc-records'>
      <h2> IMC Records  </h2>
      <table>
        <thead>
          <tr>
            <th>Date</th>
            <th>Height (cm)</th>
            <th>Weight (kg)</th>
            <th>IMC </th>
            
          </tr>
        </thead>
        <tbody>
          {imcRecords.map(record => (
            <tr key={record.imc_id}>
              <td>{record.date}</td>
              <td>{record.height}</td>
              <td>{record.weight}</td>
              <td>{record.bmi_value}</td>
        
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default GetIMC;
