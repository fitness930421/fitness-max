import React,{useState} from 'react'
import { useNavigate,Link } from 'react-router-dom';
import FormInput from '../../components/form/FormInput';
import axios from 'axios';
import './register.css'
const Register = () => {
  
  const [err,setErr]=useState("");

  const[value,setValues]=useState({
    name:"",
    lastName:"",
    username:"",
    email:"",
    password:"",
    image:"",
    date:"",
    gender:"M",
    height:0,
    weight:0,
    confirmPassword:""
}) 

  
  const navigate=useNavigate()
  
  const inputs = [
    {
      id:1,
      name:"name",
      type:"text",
      placeholder:"Name",
      errorMessage:"name should be between 3 to 6 characters and couldn't include any special character or number",
      label:"Name",
      pattern:"^[A-Za-z]{3,16}$",
      required:true
    },
    {
      id:2,
      name:"lastName",
      type:"text",
      placeholder:"Last name",
      errorMessage:"Last name should be between 3 to 6 characters and couldn't include any special character or number",
      label:"Last Name",
      pattern:"^[A-Za-z]{3,16}$",
      required:true
    },
    {
      id:3,
      name:"username",
      type:"text",
      placeholder:"username",
      errorMessage:"name should be between 3 to 6 characters and couldn't include any special character",
      label:"Username",
      pattern:"^[A-Za-z0-9]{3,16}$",
      required:true
    },
    {
      id:4,
      name:"email",
      type:"email",
      placeholder:"e-mail",
      errorMessage:"invalid email",
      label:"Email",
      required:true
    },
    {
      id:5,
      name:"date",
      type:"date",
      placeholder:"Birthday",
      label:"Birthday",
      
    },
    {
      id:6,
      name:"password",
      type:"password",
      placeholder:"Password",
      errorMessage:" password should be between 8 and 16 characters and include at least 1 number, 1 special character and 1 letter",
      label:"Password",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required:true
    },
    {
      id:7,
      name:"confirmPassword",
      type:"password",
      placeholder:"Password",
      errorMessage:" password don't match ",
      label:"Password",
      pattern: value.password,
      required:true
    },
   
    
  ]


  const handleSubmit =async(e)=>{

    e.preventDefault();
    try {
       await  axios.post("/auth/register",value)
       //await axios.post("/user/register",value)
      
       navigate("/login")
      
    }
     catch (err) {
      setErr(err.response.data);

    }
    
  }

  const onChange=(e)=>{
    setValues({...value,[e.target.name]:e.target.value})
  }

  

  return (
       <section className='maxfit__register'>
       <div className="container register__container">

       <Link to="/contact-us" className="contact__button">CONTACTER LE SERVICE CLIENT</Link>

        <div className="register__left">

        </div>
        <div className="register__right">

        <form>

          <h3>REGISTER</h3>
            
            {
              inputs.map((input)=>(
                
                <FormInput key={input.id} {...input} value={value[input.name]} onChange={onChange} />
              ))
            }
       
            <button onClick={handleSubmit}>Register</button>
            <small style={{color:'blueviolet'}}> Do you have an account ?<Link to='/login' style={{color:'green'}}> Login </Link> </small> 
          
            {err && <p className="error">{err}</p>}
        </form>
        
        </div>
        
      </div>
      </section>
     
  )
}

export default Register