import React,{useContext, useState} from 'react';
import {Link,useNavigate} from 'react-router-dom';
import FormInput from '../../components/form/FormInput';
import { authContext } from '../../context/Context';

import './login.css'


const Login = () => {
  
  const navigate=useNavigate();
  const[err,setErr]=useState("");
  
  const[value,setValues]=useState({
    username:"",
    password:"",
  }) 

  const {login}=useContext(authContext);

  // we are trying to validate data with this schema
  const inputs = [
    {
        id:1,
        name:"username",
        type:"text",
        placeholder:"username",
        errorMessage:"name should be between 3 to 6 characters and couldn't include any special character",
        label:"Username",
        pattern:"^[A-Za-z0-9]{3,16}$",
        required:true
      },
    {
        id:2,
        name:"password",
        type:"password",
        placeholder:"Password",
        errorMessage:" password should be between 8 and 16 characters and include at least 1 number, 1 special character and 1 letter",
        label:"Password",
        pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
        required:true
      }]

      // events
      const onChange=(e)=>{
        setValues(({...value,[e.target.name]:e.target.value}))
      }
      
      const handleSubmit= async(e) =>{
        e.preventDefault();
        try {
            
           login(value)
            
           navigate("/")
          
        }
         catch(err) {
          setErr(err.response.data);
         
        }
      
      }
       
return (
    <div className="container login__container">
      
      <Link to="/contact-us" className="contact__button">CONTACTER LE SERVICE CLIENT</Link>
   
        <div className="login">
        
            <form>
                <h3>Login</h3>
               {

                inputs.map((input)=>(

                    <FormInput key={input.id} {...input} value={value[input.name]} onChange={onChange}/>
                ))
               }
             
                <button onClick={handleSubmit}>Login</button> 
              
              <small style={{color:'blueviolet'}}> Don't you have an account ? <Link to='/register' style={{color:'green'}}> Register  </Link></small>
             
               <div>
               <small style={{color:'blueviolet', fontWeight:800}}>  <Link to='/passwordReset' style={{color:'green'}}> Forgot Account ?  </Link></small>
               </div>
              
              
               
            </form>
            {err && <p className="error">{err}</p>} 
        </div>
        
    </div>
)
}

export default Login