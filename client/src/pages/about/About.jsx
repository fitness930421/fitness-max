import React from 'react'
import Body from '../../components/body/Body'


const About = () => {
  return (
    <Body>
      <h3>Notre Histoire</h3>
      <p>
      Notre histoire commence avec la passion de la santé et du bien-être. Nous avons réalisé que beaucoup de gens souffrent de problèmes de santé à cause d'un mode de vie sédentaire et une alimentation déséquilibrée. C'est pourquoi nous avons créé MaxFit, 
      une application qui permet à chacun de prendre en main sa santé de manière simple et efficace.
      </p>
     
    </Body>
  )
}

export default About