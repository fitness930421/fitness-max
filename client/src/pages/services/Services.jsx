import React from 'react'
import Body from '../../components/body/Body'

const Services = () => {
  return (
    <Body>
      
      <h3>Exercices physiques adaptés</h3>
      <p>
      Nous proposons des programmes d'exercices physiques personnalisés en fonction de vos objectifs et de votre niveau de forme physique. 
      Que vous cherchiez à perdre du poids, à vous tonifier ou simplement à améliorer votre condition physique, 
      nous avons des programmes pour vous aider à atteindre vos objectifs
      </p>

      <h3>Régime alimentaire personnalisé</h3>
      <p>
      Nous élaborons des régimes alimentaires personnalisés en fonction de vos besoins et de vos préférences alimentaires. 
      Nos experts en nutrition vous guideront pour que vous puissiez manger sainement et équilibré tout en appréciant vos repas.
      </p>

      <h3>Calcul de l'IMC</h3>
      <p>
      Nous proposons un calcul de l'Indice de Masse Corporelle (IMC) pour déterminer si votre poids est dans la norme, en surpoids ou en sous-poids.
       Ce calcul est essentiel pour établir un programme d'exercices physiques et un régime alimentaire personnalisé adapté à votre situation.
      </p>
    </Body>
  )
}

export default Services