import React, { useContext, useState,useEffect } from 'react'
import './profile.css'
import axios from 'axios';
import { authContext } from '../../context/Context';
import Body from '../../components/body/Body';
import { Link, useNavigate } from 'react-router-dom';
import Session from '../../components/session/Session';
import Sessions from '../../components/session/Sessions';
import AddSession from '../../components/session/AddSession';
import UpdateSession from '../../components/session/UpdateSession';
import AddIMC from '../../components/imc/AddIMC';
import GetIMC from '../../components/imc/GetIMC';
import DeleteSessionButton from '../../components/session/DeleteSessionButton ';

const Profile = () => {


const navigate=useNavigate()

 const [weight, setWeight] = useState('');
 const [height, setHeight] = useState('');
 const [imc, setImc] = useState('');
 const[user,setUser]=useState({})

 const {currentUser, unsubscriber, getUser}=useContext(authContext);
 


const calculateImc = () => {
  const weightInKg = parseFloat(weight);
  const heightInMeters = parseFloat(height) / 100; // Convert height from centimeters to meters

  if (weightInKg && heightInMeters) {
    const imcValue = weightInKg / (heightInMeters * heightInMeters);
    setImc(imcValue.toFixed(2)); // Round the BMI value to 2 decimal places
    const imc_value= imcValue.toFixed(2)

    axios.post('/imc', { imc: imc_value })
    .then((response) => {
      console.log(response.data);
      // Handle successful BMI insertion
    })
    .catch((error) => {
      console.error(error);
      // Handle BMI insertion error
    });
  } else {
    setImc('');
  }
};

const handleDelete = async () => {
 unsubscriber();
 navigate('/login')
}




  return (
    <Body>
      <div className='user__container'>
        
      <div className='part__user' >
        <div className='part__user1'>
          
             <Link to='/editProfile'> Edit Profile</Link>   <button onClick={handleDelete}  className='subscriber'>UNSUBCRIBER</button>
        </div>  
        <div className='part__user2'>
            {currentUser &&  (<img width="600px" height="600px" src={`http://localhost:8000/upload/`+currentUser.image} alt=" profile picture " />) } 
            
            <p> Name : {currentUser.name}</p>
            <p> Last Name : {currentUser.lastName}</p>
            <p> Username : {currentUser.username}</p>
            <p> Birthday : {currentUser.date}</p>
          </div>
            {/* <Session/> */}
        </div>
        </div>
        

        <div className='user__imc'>
          
           
            <div className='elements'>
              <Link to='/addSession'> Add Session</Link>
              <Link to='/editSession'>Edit Session</Link>
              <Link to='/addImc'> Add New Imc</Link>
              <Link to='/delete'> Delete Session</Link>
              <GetIMC/>
              <Session/>
              <Sessions/>
             


            </div>

            <h2>IMC Calculator</h2>
            <div>
              <label>Weight (kg):</label>
              <input   type="text" value={weight} onChange={(e) => setWeight(e.target.value)} />
          </div>
      <div>
             <label>Height (cm):</label>
             <input  type="text" value={height} onChange={(e) => setHeight(e.target.value)} />
      </div>
      <button onClick={calculateImc}>Calculate</button>
      {imc && <div>Your IMC : {imc}</div>}

     
      </div>

      
    </Body>
  )
}

export default Profile