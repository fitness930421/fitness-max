import React from 'react'
import './contact.css'
import { useRef } from 'react'
import emailjs from 'emailjs-com'
import {TfiEmail} from 'react-icons/tfi'
import {SiTwitter} from 'react-icons/si'
import {FaLinkedin} from 'react-icons/fa'
import {BsWhatsapp} from 'react-icons/bs'
import Body from '../../components/body/Body'
const Contact = () => {

  const form =useRef();

  const sendEMail=(e)=>{
    e.preventDefault();
    emailjs.sendForm('service_d1w92ca','template_qm9q8d7',form.current,'mnQMfwlXRJmN1ypeb')
    .then((result) => {
      console.log(result.text);
  }, (error) => {
      console.log(error.text);
  });
  e.target.reset()
  }

  return (
    <Body>
      

<div className="container contact__container">

  <div className="contact_options">


    <article className='contact__option1'>

      <TfiEmail className='contact__option-icon1'/>

      <h4>Email</h4>

      <h5>maxfit@gmail.com</h5>

      <a href="mailto:enockmukolondjolo@gmail.com">Send a message</a>

    </article>




    <article className='contact__option2'>

      <SiTwitter className='contact__option-icon2'/>

      <h4>Twitter </h4>

      <h5>@maxfit</h5>

      <a href="https://twitter.com/wilondja_enock" target='_blank'>We are 24 online on twitter</a>

    </article>




    <article className='contact__option3'>

      <FaLinkedin className='contact__option-icon3'/>

      <h4>Linkedin </h4>

      <h5>MAX FIT </h5>

      <a href="https://www.linkedin.com/in/enock-mukolos-36699519a/" target='_blank'>We are 24/7 online on linkedin</a>

    </article>




    <article className='contact__option4'>

      <BsWhatsapp className='contact__option-icon4'  />

      <h4>Whatsap </h4>

      <h5>+ 1 </h5>

      <a href="https://api.whatsapp.com/send?phone=+13437770638" target='_blank'> Let's talk on whatsap</a>

    </article>




  </div>




    <form ref={form} onSubmit={sendEMail}>

      <input type="text" name='name' placeholder='your name' required/>

      <input type="email" name='email' placeholder='your email' required />

      <textarea name="message" placeholder='your message'   rows="7" required></textarea>

      <button type='submit' className='btn btn-primary'>Send your message</button>

    </form>

</div>
    </Body>
  )
}

export default Contact