import React from 'react'
import './home.css'
import imgOrdi from '../../assets/diet.png';
import { Link } from 'react-router-dom';
import Header from '../../components/header/Header';
import Body from '../../components/body/Body';

const Home = () => {
  return (
    
    <div className="App">
   
      <Header/>
      <Body>
           

        <div className="left__section">
          <h1>SANTÉ</h1>
          <h1>SPORT</h1>
          <h1>NUTRITION</h1>
          {/* <Link to="/contact-us" className="contact__button">CONTACTER LE SERVICE CLIENT</Link> */}
          
        </div>

        <div className="right__section">
          <img src={imgOrdi} className="monImage" alt="ImageOrdi" /> 
        </div>

      </Body>
      
  </div>


  )
}

export default Home